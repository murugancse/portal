<!DOCTYPE html>
<html lang="en">

<head>
  <title>
  @section('title')
    @show
  </title>
  @include('admin.layouts.head')

</head>

<body class="hold-transition login-page">

  @include('admin.layouts.header')

  @section('main-content')
  
    @show

  @include('admin.layouts.footer')

</body>

</html>
