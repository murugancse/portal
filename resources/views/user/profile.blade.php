@extends('user/app')

@section('main-content')
<div class="container">
	<div class="single">
		<div class="col-md-9 single_right">
			<div class="but_list">
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Available jobs</a></li>
						<li role="presentation"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile">Show Descriptions</a></li>
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a2.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a1.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a3.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a4.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a5.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a6.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a1.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="thumb"><a href="jobs_single.html"><img src="images/a3.jpg" class="img-responsive" alt=""/></a></div>
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="profile" aria-labelledby="profile-tab">
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
							<div class="tab_grid">
								<div class="jobs-item with-thumb">
									<div class="jobs_right">
										<div class="date">30 <span>Jul</span></div>
										<div class="date_desc">
											<h6 class="title"><a href="jobs_single.html">Front-end Developer</a></h6>
											<span class="meta">Envato, Sydney, AU</span>
										</div>
										<div class="clearfix"> </div>
										<ul class="top-btns">
											<li><a href="#" class="fa fa-plus toggle"></a></li>
											<li><a href="#" class="fa fa-star"></a></li>
											<li><a href="#" class="fa fa-link"></a></li>
										</ul>
										<p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum accusantium deleniti neque architecto vitae. <a href="jobs_single.html" class="read-more">Read More</a></p>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<ul class="pagination jobs_pagination">
				<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>
				<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><a href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>
			</ul>
		</div>
		<div class="col-md-3">
			<div class="widget_search">
				<h5 class="widget-title">Profile</h5>
				<div class="widget-content">
					<div class="priceWidgetCont">
						<div class="subscription noShadow">
							<div class="cartSlectWhite toggleSubscription">
								<p class="subscrHeading">Subscription:</p>
								<div class="toggleDisplay"><input id="display6" type="radio" name="duration" value="display6"><label class="activeLabel" id="6label" title="Get 6 Months Subscription" for="display6">6 Months</label><input id="display3" type="radio" name="duration" value="display3"><label class="" id="3label" title="Get 3 Months Subscription" for="display3">3 Months</label><input type="hidden" id="incartid" value="19"></div>
							</div>
							<div class="cartPriceTot">
								<span class="rupee28 rupeeGrey"></span><span class="resDispPrc" id="cartResDisplayPrc">$1</span>
								<p class="inclTxt">(Inclusive of all taxes)</p>
							</div>
							<div class="cartSlectWhite">
								<a href="javascript:void(0);" id="cartstatus" class="btn btn-sm btn-primary">Buy Now</a><a class="red_btnBig" href="{{ route('payment') }}" id="alreadyincart" style="display:none">Buy Now</a>
								<div><a id="offer" class="discountOffer" href="javascript:void(0);"><span class="offerTag">OFFER</span>
									Buy combo to get upto 15% off
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col_3">
				<h3>Work Experiance</h3>
				<table class="table">
					<tbody>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Senior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Middle
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Junior
							</td>
							<td>
								(56)
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col_3">
				<h3>Work Permit</h3>
				<table class="table">
					<tbody>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Full time
							</td>
						</tr>
						<tr class="unread checked">
							<td class="hidden-xs">
								<input type="checkbox" class="checkbox">
							</td>
							<td class="hidden-xs">
								Parttime
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
@endsection
