<?php

/*
|--------------------------------------------------------------------------
| Web Routes develop
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/home', 'user\UserController@index')->name('home');
//Route::get('/home', 'user\HomeController@index')->name('home');
//Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/nlogin', 'user\Auth\LoginController@showLoginForm')->name('login');
Route::post('/nlogin', 'user\Auth\LoginController@login');
Route::post('/logout', 'user\Auth\LoginController@logout')->name('logout');

Route::get('/payment', 'user\UserController@index')->name('payment');

Route::get('/padmin', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('/padmin', 'Admin\Auth\LoginController@login');
Route::get('/padmin/dashboard', 'Admin\AdminController@index')->name('admin.dashboard');
Route::get('/padmin/users', 'Admin\UserController@index')->name('users.list');


Route::get('/padmin/create', 'Admin\UserController@create');
Route::get('/padmin/index', 'Admin\UserController@index')->name('users.lists');

/* Route::get('/payment1', 'user\UserController@index')->name('home'); */

//php artisan make:controller user/UserController

